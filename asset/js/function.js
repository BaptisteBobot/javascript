function energieCinetique(masse, vitesse){
    return 0.5 * masse * Math.pow(vitesse, 2);

}